const fs = require('fs')
const path = require('path')

module.exports = function(dirName, ext, callback) {
    fs.readdir(dirName, (err, files) => {
        if (err) { 
            console.log(err)
            return callback(err) 
        }

        const filteredFiles = files.filter(file => path.extname(file).substr(1) === ext)

        callback(null, filteredFiles)
    })
}



const numbers = process.argv.slice(2)
const total = numbers.reduce((subtotal, number) => +subtotal + +number)
console.log(total)

const mymodule = require('./06-mymodule')
const dirName = process.argv[2]
const ext = process.argv[3]

mymodule(dirName, ext, (err, data) => {
    if (err) {
        console.log(err)
        return err;
    }

    data.forEach(file => {
        console.log(file)
    })
})

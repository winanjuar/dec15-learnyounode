const fs = require('fs')
const path = require('path')

const dirName = process.argv[2]
const ext = process.argv[3]

fs.readdir(dirName, (err, files) => {
    const filteredFiles = files.filter(file => path.extname(file).substr(1) === ext)

    filteredFiles.forEach(file => {
        console.log(file)
    })
})

const fs = require('fs')

const fileName1 = process.argv[2]
const fileName2 = process.argv[3]


fs.readFile(fileName1, "utf-8", (err, file1) => {
    const rows1 = file1.split('\n').length - 1;
    
    fs.readFile(fileName2, "utf-8", (err, file2) => {
        const rows2 = file2.split('\n').length - 1;
        
        console.log(rows1 + rows2)
    })
})
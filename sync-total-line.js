const fs = require('fs')

const fileName1 = process.argv[2]
const fileContent1 = fs.readFileSync(fileName1, "utf-8")
const rows1 = fileContent1.split('\n').length - 1

const fileName2 = process.argv[3]
const fileContent2 = fs.readFileSync(fileName2, "utf-8")
const rows2 = fileContent2.split('\n').length - 1

console.log(rows1 + rows2)